##Requires -RunAsAdministrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) 
{ 
	Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; 
	exit 
}
#install chocolatey package manager
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
#reload env variables
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User") 
#install nodejs and npm
choco install nodejs.install git -y
#install global tools (typescript, jspm, gulp) into path %AppData%\npm
npm install npm -g
npm install typescript jspm gulp http-server -g