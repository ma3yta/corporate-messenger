﻿import {Router, RouterConfiguration} from 'aurelia-router'

export class App {
    router: Router;

    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Aurelia';
        config.map([
            { route: ['welcome'], name: 'welcome', moduleId: 'welcome', nav: true, title: 'Welcome' },
            { route: 'movies', name: 'movies', moduleId: 'movies', nav: true, title: 'Movies' },
            { route: ['companies', ''], name: 'companies', moduleId: 'companies', nav: true, title: 'Companies' },
        ]);

        this.router = router;
    }
}