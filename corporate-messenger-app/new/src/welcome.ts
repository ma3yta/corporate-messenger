﻿import {autoinject, inject} from "aurelia-framework"
import {ensure, Validation} from "aurelia-validation"

@inject(Validation)
export class Welcome {

    @ensure(function (it) { it.isNotEmpty().hasLengthBetween(3, 10) })
    public message: string;

    public validation: any;

    constructor(validation: Validation) {
        this.validation = validation.on(this,null);
        this.message = "Hello World!";
    }

    changeMessage() {
        this.message = "Hello Aurelia!";
    }
}