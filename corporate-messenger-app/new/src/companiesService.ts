import {Company} from "./Company";

export  class CompaniesService
{
    getCompanies(): Company[]
    {
        return [
            new Company("123 456 789", "Ma3yTa Pty Ltd"),
            new Company("234 567 897", "Mivina Pty Ltd")
        ];
    }
}