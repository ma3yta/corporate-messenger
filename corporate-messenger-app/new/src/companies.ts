import {autoinject} from "aurelia-framework"
import {CompaniesService} from "./companiesService";
import {Company} from "./Company"

@autoinject()
export class Companies
{
    private companiesService: CompaniesService;
    public companies: Company[];
    
    constructor(cs: CompaniesService)
    {
        this.companiesService = cs;
    }
    
    activate()
    {
        this.companies = this.companiesService.getCompanies();
    }
    
    msg(message: string)
    {
        alert(message);
    }
    
    onStart()
    {
        alert('Start');
    }
    
    onStop()
    {
        alert('Stop');
    }
}
