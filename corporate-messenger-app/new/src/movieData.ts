﻿import {HttpClient} from "aurelia-http-client"
import {autoinject} from "aurelia-framework"

let baseUrl = "/movies.json";

@autoinject()
export class MovieData
{
    private httpClient: HttpClient;

    constructor(httpClient: HttpClient)
    {
        this.httpClient = httpClient;
    }

    getAll()
    {
        return this.httpClient.get(baseUrl).then(response=> {
            return response.content;
        }).catch(reason => console.log('${error reason}'));
    }
}