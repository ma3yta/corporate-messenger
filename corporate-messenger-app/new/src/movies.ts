﻿import {autoinject} from "aurelia-framework"
import {MovieData} from "./movieData"

@autoinject()
export class Movies
{
    private movieData: MovieData;
    public movies: any;

    constructor(movieData: MovieData)
    {
        this.movieData = movieData;
    }

    activate()
    {
        return this.movieData
                   .getAll()
                   .then(movies => this.movies = movies);
    }
}