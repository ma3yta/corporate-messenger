import {bindable} from "aurelia-framework"
import {noView, customElement} from "aurelia-templating";

@noView
@customElement("robot-body")
export class RobotBody
{
    @bindable public height: number;
    @bindable public width: number;
    
    constructor()
    {
        
    }
}