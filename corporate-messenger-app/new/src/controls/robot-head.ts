import {bindable} from "aurelia-framework"
import {noView, customElement} from "aurelia-templating";

@noView
@customElement("robot-head")
export class RobotHead
{
    @bindable public height: number;
    @bindable public width: number;
    
    constructor(){
        
    }
}