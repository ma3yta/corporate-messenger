import {bindable, autoinject} from "aurelia-framework";
import {children, child} from "aurelia-templating";
import {RobotHead} from "./RobotHead";
import {RobotBody} from "./RobotBody";

//child - if one child element
//children if few elements

export class Robot {
    
    @child("robot-head") public head: RobotHead;
    @child("robot-body") public body: RobotBody;

    bind() {
        debugger;
    }

    attached() {
        debugger;
    }
}