import {bindable} from "aurelia-framework"

export class CompanyInfo
{
    @bindable public name: string;
    @bindable public acn: string;
    @bindable public showMessage : Function;
}