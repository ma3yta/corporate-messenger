import { children, customElement } from "aurelia-framework";
import { InnerTestItem } from "./inner-test-item";

@customElement("test-screen")
export class TestScreen {
  @children("inner-test-item") testItems: InnerTestItem[] = [];

  bind() {
    debugger;
  }

  attached() {
    debugger;
  }
}