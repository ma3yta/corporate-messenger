import {bindable, autoinject, inject, customAttribute, customElement, useView} from "aurelia-framework";
import {processContent, dynamicOptions, useShadowDOM} from "aurelia-templating";

@autoinject
//@inject(Element)
//@customAttribute('my-widget')
//@customElement("widget")
//@useView("widget.html")
@useShadowDOM
export class Widget {
    
    @bindable public header: string = "Widget Header";
    @bindable public body: string = "Widget Body";
    
    private static startEvent = new CustomEvent('start', { bubbles: true });    //when events is bubbled then use delegate in template
    private static stopEvent = new CustomEvent("stop", { bubbles: false });     //when event isn't bubbled then use trigger in template
    
    constructor(private element: Element) {
       this.element.dispatchEvent(Widget.startEvent);
    }
    
    start()
    {
        this.element.dispatchEvent(Widget.startEvent);
    }
    
    stop()
    {
        this.element.dispatchEvent(Widget.stopEvent);    
    }
}