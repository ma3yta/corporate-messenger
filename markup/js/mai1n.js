var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
var pointerClick = isTouchDevice ? 'touchstart' : 'click';
var html = document.documentElement;
html.className = html.className.replace('no-js', '');

function initCustomForms() {
	jcf.setOptions('Select', {
		wrapNative: false,
		wrapNativeOnMobile: false
	});
	jcf.replaceAll();
}

function initEditCancelButtons()
{
	$('a.info-block-button-edit, a.info-block-button-cancel').on(pointerClick, function(){
		if(this.parentNode.parentNode.className.indexOf('information-new') == -1)
		{
			showHide(this.parentNode.parentNode.parentNode);
			return false;
		}
	});
}

function initPenClick()
{
	$('.edit-field').on(pointerClick, function(){
		$(this.parentNode.parentNode).find('.input-filed input').val($(this.parentNode).find('span').html());
		$(this.parentNode).addClass('value-field-inactive');
		$(this.parentNode.parentNode).find('.input-filed').addClass('input-filed-active');
		return false;
	});
	$('.input-filed .btn-submit').on(pointerClick, function(){
		changeInput(this, true);
		return false;
	});
	$('.input-filed .btn-cancel').on(pointerClick, function(){
		changeInput(this, false);
		return false;
	});
	$('.input-filed').find('input').on('keyup', function(e){
		var key;
		if(window.event)
		{
			key=window.event.keyCode;
		}
		else if(e)
		{
			key=e.which;
		}
		if(key == 13)
		{
			try{
				changeInput($(this.parentNode.parentNode).find('.btn-submit')[0], true);
			}
			catch(e)
			{}
		}
	});
}

function changeInput(el, change)
{
	var pd = $(el.parentNode);
	var vd = $(el.parentNode.parentNode).find('.value-field');
	if(change){
		vd.find('span').html(pd.find('input').val());
	}
	vd.removeClass('value-field-inactive');
	pd.removeClass('input-filed-active');
}

function initTableRowClick()
{
	$('.table-row .table-cell').on(pointerClick, function(){
		if($(this).index() != 5 && this.parentNode.className.indexOf('table-head') == -1)
		{
			var tr = this.parentNode;
			var selfClick = tr.className.indexOf('active') != -1;
			$('.table-row').removeClass('active');
			$('.inner-block').slideUp();
			if(!selfClick)
			{
				$(tr).addClass('active');
				$(tr.parentNode).children().eq($(tr).index() + 1).slideDown();
			}
			return false;
		}
	});

}

function init2Buttons()
{
	$('.information-new a.info-block-button-cancel').on(pointerClick, function(){
		$(this.parentNode.parentNode).hide();
		return false;
	});
	$('a.info-block-button').each(function(){
		if(this.innerHTML.indexOf('Create New Officer') != -1 || this.innerHTML.indexOf('Create New Shareholder') != -1)
		{
			$(this).on(pointerClick, function(){
				$(this.parentNode.parentNode.parentNode).find('.information-new').show();
				return false;
			});
		}
	});
}

function showHide2(parent)
{
	var _blocks = $(parent).find('div.info-block-edit');
	var _buttons = $(parent).find('div.info-block-buttons');
	var _hiddenBlocks = $(parent).find('div.info-block-hidden');
	var _hiddenButtons = $(parent).find('div.info-block-buttons-hidden');
	_blocks.each(function(){
		$(this).removeClass('info-block-edit').addClass('info-block-hidden');
	});
	_buttons.each(function(){
		$(this).removeClass('info-block-buttons').addClass('info-block-buttons-hidden');
	});
	_hiddenBlocks.each(function(){
		$(this).addClass('info-block-edit').removeClass('info-block-hidden');
	});
	_hiddenButtons.each(function(){
		$(this).addClass('info-block-buttons').removeClass('info-block-buttons-hidden');
	});
}

function showHide(parent)
{
	var _blocks = $(parent).find('div.info-block-edit');
	var _buttons = $(parent).find('div.info-block-buttons');
	var _hiddenBlocks = $(parent).find('div.info-block-hidden');
	var _hiddenButtons = $(parent).find('div.info-block-buttons-hidden');
	_blocks.each(function(){
		$(this).removeClass('info-block-edit').addClass('info-block-hidden');
	});
	_buttons.each(function(){
		$(this).removeClass('info-block-buttons').addClass('info-block-buttons-hidden');
	});
	_hiddenBlocks.each(function(){
		$(this).addClass('info-block-edit').removeClass('info-block-hidden');
	});
	_hiddenButtons.each(function(){
		$(this).addClass('info-block-buttons').removeClass('info-block-buttons-hidden');
	});
}


function initBasket()
{
	$('a.basket').on(pointerClick, function(){
		var row = this.parentNode.parentNode;
		row.parentNode.removeChild(row);
		return false;
	});
}

function initMessages() {
	var messagesTray = $('.message-gallery'),
		messagesTraySlideset = messagesTray.find('.slideset');

	isTrayCarouselEmpty() ? messagesTray.hide() : initTrayCarousel();

	$('.message-row, .message-tray').fancybox({
		closeBtn: false,
		padding: 0,
		helpers: {
			overlay: {
				closeClick: false
			}
		},
		afterShow: function () {
			var fancyInstance = this,
				elem = fancyInstance.element,
				close = fancyInstance.inner.find('.close'),
				roll = fancyInstance.inner.find('.roll'),
				full = fancyInstance.inner.find('.full');

			if(elem.hasClass('message-row')) {
				close.on('click.fancy', function (e) {
					e.preventDefault();
					$.fancybox.close();
				});

				roll.on('click.fancy', function (e) {
					e.preventDefault();

					if(!$('.message-tray[data-message-id=' + elem.data('messageId') + ']').length) {
						messagesTraySlideset.append(
							createTrayCarouselItem(elem.data('color'), elem.data('fancyboxHref'), elem.data('subject'), elem.data('time'), elem.data('messageId'))
						);
						reinitTrayCarousel();
					}

					$.fancybox.close();
				});
			}
			else if(elem.hasClass('message-tray')) {
				close.on('click.fancy', function (e) {
					e.preventDefault();

					elem.parents('.slide').remove();
					reinitTrayCarousel();

					$.fancybox.close();
				});

				roll.on('click.fancy', function (e) {
					e.preventDefault();
					$.fancybox.close();
				});
			}
		},
		beforeClose: function () {
			var fancyInstance = this,
				close = this.inner.find('.close'),
				roll = this.inner.find('.roll'),
				full = this.inner.find('.full');

			close.off('.fancy');
			roll.off('.fancy');
		}
	});

	function initTrayCarousel() {
		if(messagesTray.is(':hidden')) messagesTray.show();

		messagesTray.scrollGallery({
			mask: '.mask',
			slider: '.slideset',
			slides: '.slide',
			btnPrev: '.btn-prev',
			btnNext: '.btn-next',
			pagerLinks: '.pagination li',
			stretchSlideToMask: false,
			maskAutoSize: true,
			autoRotation: false,
			switchTime: 3000,
			animSpeed: 500,
			step: 1
		});
	}

	function destroyTrayCarousel() {
		messagesTray.data('ScrollGallery').destroy();
	}

	function reinitTrayCarousel() {
		try {
			destroyTrayCarousel();
		} catch(e) {}

		if(!isTrayCarouselEmpty()) {
			messagesTray.hide();
			return;
		}

		initTrayCarousel();
	}

	function isTrayCarouselEmpty() {
		return !!messagesTraySlideset.find('.slide').length;
	}

	function createTrayCarouselItem(color, href, subject, time, id) {
		var html = '\
			<div class="slide"> \
				<a class="slide-wrap message-tray message-' + color +'" href="' + href +'" data-message-id="' + id + '"> \
					<span>' + subject + '</span> \
					<time class="date">' + time + '</time> \
				</a> \
			</div>';

		return $(html);
	}
}

$(document).ready(function(){
	$('.inner-block').hide();
	initEditCancelButtons();
	init2Buttons();
	initPenClick();
	initTableRowClick();
	initBasket();
	//initCustomForms();
	initMobileNav();
	initMessages();
});